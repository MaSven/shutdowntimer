/**
 * 
 */
/**
 * @author eiamnacken
 *
 */
module space.smarquardt.shutdown.gui {
	exports space.smarquardt.shutdown.gui;

	requires javafx.base;
	requires javafx.controls;
	requires javafx.graphics;
	requires space.smarquardt.shutdown.cli;
}