package space.smarquardt.shutdown.gui;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import space.smarquardt.shutdowntimer.cli.ShutdownTimer;

public class Main extends Application {

	private ShutdownTimer timer;

	private StringProperty remainingTime;

	private StringProperty input;

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.timer = new ShutdownTimer();
		GridPane gridPane = new GridPane();
		gridPane.setAlignment(Pos.CENTER);
		gridPane.setHgap(10.0);
		gridPane.setVgap(10.0);
		gridPane.setPadding(new Insets(25, 25, 25, 25));
		TextField timeInput = new TextField("Zeit hier eingeben");
		this.input = timeInput.textProperty();
		Button setNewTime = new Button("Starte countdonw");
		setNewTime.setOnAction(event -> {
			shutdownPressed();
		});
		Button abortShutdown = new Button("Herunterfahren abbrechen");
		abortShutdown.setOnAction(e -> {
			this.timer.abortShutdown();
			this.remainingTime.set("00:00:00");
		});
		gridPane.add(timeInput, 0, 0);
		gridPane.add(setNewTime, 1, 0);
		gridPane.add(abortShutdown, 1, 1);
		Label descriptionTime = new Label("00:00:00");
		gridPane.add(descriptionTime, 2, 0);
		this.remainingTime = descriptionTime.textProperty();
		Scene scene = new Scene(gridPane, 600, 400);
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	private void shutdownPressed() {

		LocalTime localTimeOfInput = LocalTime.parse(this.input.get(), DateTimeFormatter.ofPattern("HH:mm:ss"));
		Duration shutdownTime = Duration.between(LocalTime.now(),
				LocalTime.now().plusSeconds(localTimeOfInput.toSecondOfDay()));
		this.timer.startShutdown(shutdownTime, duration -> {
			long inSeconds = duration.toSecondsPart();
			long inMinutes = duration.toMinutesPart();
			long inHours = duration.toHoursPart();
			System.out.println(duration.toString());
			Platform.runLater(() -> {
				this.remainingTime.set(String.format("%d:%02d:%02d", inHours, inMinutes, inSeconds));
			});
		});
	}

	public static void main(String[] args) {
		launch(args);
	}

}
