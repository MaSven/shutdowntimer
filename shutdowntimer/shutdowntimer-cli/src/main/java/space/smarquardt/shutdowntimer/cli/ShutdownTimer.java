package space.smarquardt.shutdowntimer.cli;

import java.io.IOException;
import java.time.Duration;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class ShutdownTimer {
		private final ProcessBuilder currentBuilder;
		
		private Duration remainingTime;
		

		
		private ScheduledFuture<?> shutdownProcess;
		
		private ProcessHandle shutdownHandle;

		public ShutdownTimer() {
			super();
			this.currentBuilder =  new ProcessBuilder();
			
		}
		
		public void startShutdown(Duration timeTilShutdown,Consumer<Duration> remainingTimeChanged) {
			this.currentBuilder.command("shutdown","/s","/t ",String.valueOf(timeTilShutdown.getSeconds()));
			this.remainingTime=timeTilShutdown;

				executeCommand();
				Runnable timeReducer = ()->{
					this.remainingTime= this.remainingTime.minusSeconds(1);
					remainingTimeChanged.accept(this.remainingTime);
				};
				var executorService = Executors.newScheduledThreadPool(1);
				this.shutdownProcess = executorService.scheduleAtFixedRate(timeReducer, 1,1, TimeUnit.SECONDS);
				
		}
		
		private void executeCommand() {
			try {
				Process shutdownProcess = this.currentBuilder.inheritIO().start();
				this.shutdownHandle = shutdownProcess.toHandle();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public void abortShutdown() {
			this.shutdownHandle.destroyForcibly();
			this.shutdownProcess.cancel(true);
			this.currentBuilder.command("shutdown","/a");
			this.remainingTime=Duration.ZERO;
			this.executeCommand();

			
		}
		
		
}
